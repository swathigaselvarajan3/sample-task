document.addEventListener('DOMContentLoaded', function() {
    const select1 = document.getElementById('selectinput1');
    const select2 = document.getElementById('selectinput2');
    const select3 = document.getElementById('selectinput3');
    const actions1 = document.getElementById('actions1');
    const actions2 = document.getElementById('actions2');
    const actions3 = document.getElementById('actions3');
    const pair = document.getElementById('pair');
    const two_pair = document.getElementById('two_pair');
    const three_pair = document.getElementById('three_pair');

    // Add event listeners to radio buttons
    select1.addEventListener('change', function() {
      actions1.style.display = 'block';
      actions2.style.display = 'none';
      actions3.style.display = 'none';
      pair.classList.add('box1'); 
      two_pair.classList.remove('box1'); 
      three_pair.classList.remove('box1'); 
    });

    select2.addEventListener('change', function() {
      actions1.style.display = 'none';
      actions2.style.display = 'block';
      actions3.style.display = 'none';
      two_pair.classList.add('box1'); 
      pair.classList.remove('box1'); 
      three_pair.classList.remove('box1'); 
    });

    select3.addEventListener('change', function() {
      actions1.style.display = 'none';
      actions2.style.display = 'none';
      actions3.style.display = 'block';
      three_pair.classList.add('box1'); 
      pair.classList.remove('box1'); 
      two_pair.classList.remove('box1'); 
    });
    // const getValueButton = document.getElementById('selectinput1');
  
    // getValueButton.addEventListener('click', function() {
    //     const actions = document.getElementById('actions');
    //     actions.style.display = 'block';
    // });
  });
  